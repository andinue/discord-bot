const axios = require("axios");
const BASEURL = "https://screeenly.cpn.so/api/v1/fullsize";

exports.Plugin = {
  command: ["스샷"],
  proc: function (message, args) {

    if (args[0]) {
      let target_url = args[0];
      if (!(target_url.startsWith("http://") || target_url.startsWith("https://"))) {
        target_url = "http://" + target_url;
      }
      axios({
        url: BASEURL,
        method: 'post',
        data: {
          key: "RxIcPuiof4kCLKWNU7PC7jFB64KLTMBTCGXU1qajJVUqdVpEvQ",
          url: target_url
        }
      }).then((response) => {
        message.reply(`screenshot : ${response.data.path}`);
      })
    }
  }
};

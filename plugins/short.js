const axios = require("axios");
const BASEURL = "https://cpn.so/yourls-api.php?signature=b0bbca4b43";

exports.Plugin = {
  command: ["숏"],
  proc: function (message, args) {

    if (args[0]) {
      let target_url = args[0];
      if (!(target_url.startsWith("http://") || target_url.startsWith("https://"))) {
        target_url = "http://" + target_url;
      }
      let _url = BASEURL + "&action=shorturl&format=json&url=" + target_url;
      if (args[1]) _url = _url + "&keyword=" + args[1]
      axios({
        url: _url,
        method: 'get',
      }).then((response) => {
        message.reply(`shorturl : ${response.data.shorturl}`);
      })
    }
  }
};

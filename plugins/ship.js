const axios = require("axios");
const cheerio = require("cheerio");
const { MessageEmbed } = require('discord.js');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./ship.db');
const shiplists = require('../shiplist.json');


const replaceStr = {
  "Manufacturer": "제조사",
  "Role": "용도",
  "Size": "함선크기",
  "Crew": "승선인원",
  "Series": "시리즈",
  "Cargo capacity": "화물공간",
  "Stowage space": "수납공간",
  "Production state": "출시상태",
  "Buy": "인게임가격",
  "Rent (1 day)": "렌트비용",
  "Claim time": "클레임시간",
  "Expedite": "재촉시간",
  "Expedite fee": "재촉비용",
  "Standalone": "스탠드얼론",
  "Original": "오리지날",
  "Availability": "구입가능여부",
  "Length": "전장",
  "Beam": "전폭",
  "Height": "전고",
  "Combat speed": "가속력",
  "Max speed": "최고속도",
  "Mass": "질량",
  "Introduction": "소개",
  "base": "기본",
  "cost": "비용",
  "insurance": "보험",
  "pledge": "서약",
  "specifications": "스펙",
  "lore": "로어",

  "Drake Interplanetary (DRAK)": "DRAK",
  "Anvil Aerospace (ANVL)": "ANVL",
  "Musashi Industrial and Starflight Concern (MISC)": "MISC",
  "Roberts Space Industries (RSI)": "RSI",
  "Aegis Dynamics (AEGS)": "AEGS",
  "Crusader Industries (CRUS)": "CRUS",
  "Vanduul Clans (VNCL)": "VNCL",

  "Time-limited sales": "기간한정",
  "Always available": "상시판매",
  "Not available for sale": "판매불가",
  "Not available": "구매불가",
  "Unknown": "알 수 없음",

  "Active for Squadron 42": "스쿼드론",

  "Carrier": "항공모함",
  "Escort Carrier": "호위항모",
  "Transport": "수송",
  "Support - Medical": "의료"
}

const divider = {
  "base": ["Manufacturer", "Role", "Size", "Crew", "Series", "Cargo capacity", "Stowage space", "Production state"],
  "cost": ["Buy", "Rent (1 day)"],
  "insurance": ["Claim time", "Expedite", "Expedite fee",],
  "pledge": ["Standalone", "Original", "Availability"],
  "specifications": ["Length", "Beam", "Height", "Combat speed", "Max speed", "Mass"],
  "lore": ["Introduction"],
};

var parseShip = function (message, link, name) {
  axios({
    url: link,
    method: 'get',
  }).then((response) => {
    let $ = cheerio.load(response.data);

    let image = $(".infobox-image a img").attr("src");
    let shipinfo = []
    $(".infobox-data").each(function () {
      if ($(this).find("td").find("li").length > 0) {
        let _datalist = [];
        $(this).find("td").find("li").each(function () {
          _datalist.push($(this).text());
        })
        shipinfo[$(this).find("th").text()] = _datalist.join("\n");
      } else {
        shipinfo[$(this).find("th").text()] = $(this).find("td").text().replace(" / ", "\n");
      }
    })

    const exampleEmbed = new MessageEmbed().setColor('#0099ff')
      .setTitle(name)
      .setURL(link)
      .setImage('https://starcitizen.tools' + image)

    for (const key in divider) {
      // exampleEmbed.addField('\u200B', '\u200B');
      divider[key].forEach(_data => {
        if (shipinfo[_data]) {
          exampleEmbed.addField(replaceStr[_data] ? replaceStr[_data] : _data, replaceStr[shipinfo[_data]] ? replaceStr[shipinfo[_data]] : shipinfo[_data], true)
        }
      });
    }
    message.reply({ embeds: [exampleEmbed] });
  })
}

var parseShipDB = async (message, search_key, _force) => {

  let _name = await getShipAlias(search_key);
  let _base = await getShipBase(_name);
  let _data = await getShipData(_name);
  if (Object.keys(_data).length == 0 || _force) {
    _data = await updateShipData(_name);
  }

  const exampleEmbed = new MessageEmbed().setColor('#0099ff')
    .setTitle(_name)
    .setURL(_base.link)
    .setImage('https://starcitizen.tools' + _data.image)

  for (const key in divider) {
    // exampleEmbed.addField('\u200B', '\u200B');
    divider[key].forEach(_div => {
      if (_data[_div]) {
        exampleEmbed.addField(replaceStr[_div] ? replaceStr[_div] : _div,
          replaceStr[_data[_div]] ? replaceStr[_data[_div]] : _data[_div],
          true
        )
      }
    });
  }
  message.reply({ embeds: [exampleEmbed] });
}

var getShipAlias = (skey) => {
  return new Promise((resolv, reject) => {
    db.all(`SELECT * FROM ships_alias WHERE UPPER(aliasname) LIKE UPPER('${skey}') LIMIT 1`, (err, row) => {
      // if (err) reject();
      if (err) console.log(err);
      if (row.length) {
        resolv(row[0]['name']);
      }
    });
  })
}

var getShipBase = (name) => {
  return new Promise((resolv, reject) => {
    db.all(`SELECT * FROM ships WHERE name LIKE '${name}' LIMIT 1`, (err, row) => {
      // if (err) reject();
      if (row.length) {
        let _shipdata = row[0];
        resolv(_shipdata);
      }
    });
  })
}

var getShipData = (name) => {
  return new Promise((resolv, reject) => {
    let _result = {}
    db.all(`SELECT metakey, metavalue FROM ships_data WHERE name LIKE '${name}'`, (err, row) => {
      row.forEach(data => {
        _result[data['metakey']] = data.metavalue
      });
      resolv(_result);
    })
  });
}

var updateShipData = (name) => {
  return new Promise(async (resolv, reject) => {
    let _base = await getShipBase(name);
    axios({
      url: _base['link'],
      method: 'get',
    }).then((response) => {
      let $ = cheerio.load(response.data);

      let shipinfo = {}
      $(".infobox-data").each(function () {
        if ($(this).find("td").find("li").length > 0) {
          let _datalist = [];
          $(this).find("td").find("li").each(function () {
            _datalist.push($(this).text());
          })
          shipinfo[$(this).find("th").text()] = _datalist.join("\n");
        } else {
          shipinfo[$(this).find("th").text()] = $(this).find("td").text().replace(" / ", "\n");
        }
      })
      shipinfo['image'] = $(".infobox-image a img").attr("src");

      if (Object.keys(shipinfo).length) {
        db.serialize(() => {
          db.run(`DELETE FROM ships_data WHERE name='${name}'`);

          let insertdata = db.prepare("INSERT INTO ships_data (name, metakey, metavalue) VALUES (?, ?, ?)");
          for (const key in shipinfo) {
            insertdata.run(name, key, shipinfo[key]);
          }
          insertdata.finalize();
          resolv(shipinfo);
        });
      } else {
        reject();
      }
    })
  })
}

var insertShipAlias = (source, target) => {
  return new Promise((resolv, reject) => {
    (async () => {
      let _base = await getShipBase(source);
      if (Object.keys(_base).length !== 0) {
        let insertalias = db.prepare("INSERT INTO ships_alias (name, aliasname) VALUES (?, ?)");
        insertalias.run(_base.name, target);
        insertalias.finalize();
        resolv();
      }
    })()
  })
}

var removeShipAlias = (target) => {
  return new Promise((resolv, reject) => {
    (async () => {
      db.run(`DELETE FROM ships_alias WHERE aliasname='${target}'`);
      resolv();
    })()
  })
}







exports.Plugin = {
  command: ["ship", "쉽", "쉽강", "배", "배강", "함선", "쉽연결", "쉽연결해제"],
  proc: function (message, args, command) {
    if (command == "쉽연결") {
      let params = args.join(" ");
      params = params.split(`"`)
      let source = params[1].replace(/\"/gi, "")
      let target = params[3].replace(/\"/gi, "")

      if (source || target) {
        insertShipAlias(source, target);
      }
    } else if (command == "쉽연결해제") {
      let params = args.join(" ");
      params = params.split(`"`)
      let target = params[1].replace(/\"/gi, "")

      if (target) {
        removeShipAlias(target);
      }
    } else {
      let search_key = args.join(" ");
      if (search_key) {
        console.log(`Called : ${search_key}`);
        let _hit = false;
        let _force = false;

        if (["쉽강", "배강"].indexOf(command) !== -1) _force = true;

        parseShipDB(message, search_key, _force);

        // 예전코드
        // shiplists.forEach(_ship => {
        //   _ship.alias.forEach(_alias => {
        //     if (_alias.toLowerCase() == search_key.toLowerCase() && !_hit) {
        //       _hit = true;
        //       parseShip(message, _ship.link, _ship.name);
        //     }
        //   });
        // });

      }
    }
  }
}
// 디비 초기화용
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./ship.db');
const shiplists = require('../shiplist.json');
const axios = require("axios");
const cheerio = require("cheerio");

db.serialize(() => {
  db.run(`CREATE TABLE IF NOT EXISTS ships (
    no INTEGER PRIMARY KEY AUTOINCREMENT,
    link TEXT,
    name TEXT,
    manufacturer TEXT,
    role TEXT,
    size TEXT,
    mass TEXT,
    production_state TEXT,
    price TEXT
  )`);

  db.run(`CREATE TABLE IF NOT EXISTS ships_alias (
    no INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    aliasname TEXT
  )`);

  db.run(`CREATE TABLE IF NOT EXISTS ships_data (
    no INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    metakey TEXT,
    metavalue TEXT
  )`);


  // db.run(`DELETE FROM ships WHERE 1`);
  // db.run(`DELETE FROM ships_alias WHERE 1`);

  // const insertship = db.prepare("INSERT INTO ships (link, name, manufacturer, role, size, mass, production_state, price) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
  // const insertalias = db.prepare("INSERT INTO ships_alias (name, aliasname) VALUES (?, ?)");
  // shiplists.forEach(ship => {
  //   insertship.run(
  //     ship['link'],
  //     ship['name'],
  //     ship['manufacturer'],
  //     ship['role'],
  //     ship['size'],
  //     ship['mass'],
  //     ship['production_state'],
  //     ship['price']
  //   );

  //   ship.alias.forEach(_alias => {
  //     insertalias.run(
  //       ship['name'],
  //       _alias
  //     );
  //   });
  // })
  // insertship.finalize();
  // insertalias.finalize();

});

// db.close();
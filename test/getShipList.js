// 초기 데이터 긁어오기

const axios = require("axios");
const cheerio = require("cheerio");
const fs = require('fs');

axios({
  url: 'https://starcitizen.tools/Category:Ships',
  method: 'get',
}).then((response) => {
  let $ = cheerio.load(response.data);
  let shiplist = []
  $(".mw-parser-output .wikitable tr").each(function () {
    let link = "https://starcitizen.tools" + $(this).find("td:eq(0) a").attr("href");
    let name = $(this).find("td:eq(0)").text().trim();
    let manufacturer = $(this).find("td:eq(1)").text().trim();
    let role = $(this).find("td:eq(2)").text().trim();
    let size = $(this).find("td:eq(3)").text().trim();
    let mass = $(this).find("td:eq(4)").text().trim();
    let production_state = $(this).find("td:eq(5)").text().trim();
    let price = $(this).find("td:eq(6)").text().trim();
    let _ship = {
      "link": link,
      "name": name,
      "manufacturer": manufacturer,
      "role": role,
      "size": size,
      "mass": mass,
      "production_state": production_state,
      "price": price,
      "alias": [name]
    };
    shiplist.push(_ship);


  })
  const jsonContent = JSON.stringify(shiplist);


  fs.writeFile("./shiplist.json", jsonContent, 'utf8', function (err) {
    if (err) {
      return console.log(err);
    }

    console.log("The file was saved!");
  });

})
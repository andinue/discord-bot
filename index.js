// https://discord.com/api/oauth2/authorize?client_id=403812722619318273&permissions=16384&scope=bot
const axios = require("axios");
const cheerio = require("cheerio");
const Discord = require("discord.js");
const config = require("./config.json");
const prefix = "!";

const plugin_init = ['ping', 'ship', 'short', 'screenshot'
  // , 'shiplist'
];
var plugins = [];
plugin_init.forEach(plugin_file => {
  let loadfile = './plugins/' + plugin_file + '.js';
  delete require.cache[require.resolve(loadfile)];

  let plugin = require(loadfile);
  plugins.push(plugin.Plugin);
});

const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES"] })
client.login(config.BOT_TOKEN);

client.on("messageCreate", function (message) {
  if (message.author.bot) return;
  if (!message.content.startsWith(prefix)) return;

  const commandBody = message.content.slice(prefix.length);
  const args = commandBody.split(' ');
  const command = args.shift().toLowerCase();

  plugins.forEach(plugin => {
    if (plugin.command.indexOf(command) !== -1) {
      plugin.proc(message, args, command);
    }
  });
});
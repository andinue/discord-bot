#/bin/bash

trap "stop; exit 0;" SIGTERM SIGINT
stop()
{
  echo "SIGTERM caught..."
  pm2 stop imabot
  echo "Terminated."
  exit
}

cd /app
if [ ! -f "/app/config.json" ]; then
  echo "{ \"BOT_TOKEN\": \"${BOT_TOKEN}\" }" >> /app/config.json
fi

pm2-runtime run.json

FROM node:latest

ENV BOT_TOKEN=

WORKDIR /app
COPY . /app
RUN apt update; \
    npm install -g pm2 && npm install

COPY entrypoint.sh /usr/bin/
CMD entrypoint.sh